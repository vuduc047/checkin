CREATE TABLE "users" (
                         id bigserial PRIMARY KEY,
                         created_by varchar(50) NULL,
                         created_date timestamp NULL,
                         last_modified_by varchar(50) NULL,
                         last_modified_date timestamp NULL,
                         first_name varchar(50) NULL,
                         last_name varchar(50) NULL,
                         status varchar(50) NOT NULL
);