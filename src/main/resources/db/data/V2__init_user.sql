Insert into "users" (id, created_by, created_date, last_modified_by, last_modified_date, first_name, last_name, status) values
(1, 'SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM',CURRENT_TIMESTAMP, 'Vu', 'Duke', 'NONE'),
(2, 'SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM',CURRENT_TIMESTAMP, 'Tran', 'Titus', 'NONE'),
(3, 'SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM',CURRENT_TIMESTAMP, 'Nguyen', 'Keppler', 'NONE'),
(4, 'SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM',CURRENT_TIMESTAMP, 'Tran', 'Lucas', 'NONE'),
(5, 'SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM',CURRENT_TIMESTAMP, 'Doan', 'Hoang', 'NONE'),
(6, 'SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM',CURRENT_TIMESTAMP, 'Nguyễn', 'Mark', 'NONE'),
(7, 'SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM',CURRENT_TIMESTAMP, 'Hoàng', 'Mơ', 'NONE'),
(8, 'SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM',CURRENT_TIMESTAMP, 'Nguyen', 'Teddy', 'NONE'),
(9, 'SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM',CURRENT_TIMESTAMP, 'Nguyen', 'Tobias', 'NONE'),
(10, 'SYSTEM', CURRENT_TIMESTAMP, 'SYSTEM',CURRENT_TIMESTAMP, 'Duong', 'Paris', 'NONE');

select setval('check_in.users_id_seq', 10);


