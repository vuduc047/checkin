package com.techvify.checkin.common;

public enum FacialRecognitionStatus {
    PENDING,
    TRAINED,
    NONE
}
