package com.techvify.checkin.entity;

import com.techvify.checkin.common.FacialRecognitionStatus;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Getter
@Setter
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Column(
            name = "created_by",
            length = 50,
            updatable = false
    )
    private String createdBy;
    @Column(
            name = "created_date",
            updatable = false
    )
    private Instant createdDate = Instant.now();
    @Column(
            name = "last_modified_by",
            length = 50
    )
    private String lastModifiedBy;
    @Column(
            name = "last_modified_date"
    )
    private Instant lastModifiedDate = Instant.now();
    @Column(
            name = "last_name",
            length = 50
    )
    private String lastName;
    @Column(
            name = "first_name",
            length = 50
    )
    private String firstName;

    @Enumerated(EnumType.STRING)
    private FacialRecognitionStatus status;
}
