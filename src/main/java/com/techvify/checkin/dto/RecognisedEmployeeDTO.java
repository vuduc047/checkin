package com.techvify.checkin.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class RecognisedEmployeeDTO {
    @JsonProperty
    private String partId;
    @JsonProperty
    private String probability;
    @JsonProperty
    private String emotion;

    public String getPartId() {
        return partId;
    }

    public void setPartId(String partId) {
        this.partId = partId;
    }


    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    public Float getProbability() {
        if (probability == null) {
            return 0F;
        }
        return Float.parseFloat(probability);
    }
}
