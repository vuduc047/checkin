package com.techvify.checkin.dto;

import com.techvify.checkin.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserMapper {
    public static UserDto toUserDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .build();
    }

    public static List<UserDto> toUserDTOList(List<User> users) {
        List<UserDto> userDTOS = new ArrayList<>();
        users.forEach((user -> userDTOS.add(toUserDto(user))));
        return  userDTOS;
    }
}
