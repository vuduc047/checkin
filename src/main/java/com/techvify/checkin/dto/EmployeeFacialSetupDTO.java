package com.techvify.checkin.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class EmployeeFacialSetupDTO {
    @JsonProperty
    private List<String> imgs;
    @JsonProperty
    private Long secondsTime;

    private String timeVerify;
}
