package com.techvify.checkin.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserImageDto {
    @JsonProperty
    private Long employeeId;
    @JsonProperty
    private String employeeName;
    @JsonProperty
    private List<String> imgs;
    @JsonProperty
    private Long secondsTime;
}
