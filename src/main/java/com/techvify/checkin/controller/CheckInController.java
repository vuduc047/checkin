package com.techvify.checkin.controller;

import com.techvify.checkin.common.Constant;
import com.techvify.checkin.common.FacialRecognitionStatus;
import com.techvify.checkin.dto.EmployeeFacialSetupDTO;
import com.techvify.checkin.dto.RecognisedUserDTO;
import com.techvify.checkin.dto.UserDto;
import com.techvify.checkin.entity.User;
import com.techvify.checkin.exception.ValidationException;
import com.techvify.checkin.service.FacialRecognition;
import com.techvify.checkin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/employee")
public class CheckInController {

    @Autowired
    private UserService userService;

    @Autowired
    private FacialRecognition facialRecognition;

    @PostMapping("/{id}/facial-recognition/setup")
    public ResponseEntity<String> setupEmployeeForFacialRecognition(@PathVariable Long id,
                                                                    @RequestBody EmployeeFacialSetupDTO employeeFacialSetupDTO) {
        if (employeeFacialSetupDTO.getImgs().size()
                < Constant.NUMBER_OF_EMPLOYEE_IMAGES_FOR_SETUP) {
            throw new ValidationException("User supplied invalid data");
        }

        User user = userService.getUserById(id);

        List<FacialRecognitionStatus> facialRecognitionStatuses =
                Arrays.asList(FacialRecognitionStatus.PENDING, FacialRecognitionStatus.TRAINED);
        String result = "";
        if (facialRecognitionStatuses.contains(user.getStatus())) {
            result = "cút";
            return ResponseEntity.ok(result);
        }
        result = facialRecognition.setupEmployeeForFacialRecognition(user, employeeFacialSetupDTO.getImgs());
//        if (result.equals("OK")) {
//            imageVerifyService.saveImageForSetup(employeeFacialSetupDTO.getImgs(), accountEmployee.getEmployee());
//        }
        userService.updateUser(user, FacialRecognitionStatus.PENDING);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/{id}/setup/import-images")
    public ResponseEntity<String> importFile(@RequestParam("files") MultipartFile[] multipartFiles,
                                             @PathVariable Long id) {
        String res = facialRecognition.importImageForSetup(multipartFiles, id);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PostMapping("/facial-recognition/verify")
    public ResponseEntity<RecognisedUserDTO> verifyEmployeeByFacialRecognition(@RequestBody EmployeeFacialSetupDTO employeeFacialVerifyDTO) {
        RecognisedUserDTO recognisedUserDTO =
                facialRecognition.verifyEmployeeByFacialRecognition(employeeFacialVerifyDTO);
        return new ResponseEntity<>(recognisedUserDTO, HttpStatus.OK);
    }

    @PostMapping("/facial-recognition/web-verify")
    public ResponseEntity<RecognisedUserDTO> verifyEmployeeByFacial(@RequestParam("file") MultipartFile file) {
        EmployeeFacialSetupDTO employeeFacialVerifyDTO = new EmployeeFacialSetupDTO();
        String imgBase64 = "";
        try {
            imgBase64 = new String(Base64.getEncoder().encode(file.getBytes()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        employeeFacialVerifyDTO.setImgs(Collections.singletonList(imgBase64));
        Long currentDatetime =  System.currentTimeMillis();
        employeeFacialVerifyDTO.setTimeVerify(currentDatetime.toString());
        RecognisedUserDTO recognisedUserDTO =
                facialRecognition.verifyEmployeeByFacialRecognition(employeeFacialVerifyDTO);
        return new ResponseEntity<>(recognisedUserDTO, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity addUser(@RequestBody UserDto user) {
        UserDto userDto = userService.saveUser(user);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<List<UserDto>> allUser() {
        List<UserDto> users = userService.getAllUser();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }
}
