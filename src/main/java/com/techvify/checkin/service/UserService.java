package com.techvify.checkin.service;

import com.techvify.checkin.common.FacialRecognitionStatus;
import com.techvify.checkin.dto.UserDto;
import com.techvify.checkin.dto.UserMapper;
import com.techvify.checkin.entity.User;
import com.techvify.checkin.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User getUserById(Long id) {
        User user = userRepository.findUserById(id);
        return user;
    }

    public void updateUser(User user, FacialRecognitionStatus status) {
        user.setStatus(status);
        userRepository.save(user);
    }

    public UserDto saveUser(UserDto userDto) {
        User user = User.builder()
                .firstName(userDto.getFirstName())
                .lastName(userDto.getLastName())
                .status(FacialRecognitionStatus.NONE)
                .build();
        user = userRepository.save(user);
        userDto.setId(user.getId());
        return userDto;
    }

    public List<UserDto> getAllUser(){
        List<User> users = userRepository.findAll();
        return UserMapper.toUserDTOList(users);
    }
}
