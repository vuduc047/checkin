package com.techvify.checkin.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.techvify.checkin.dto.EmployeeFacialSetupDTO;
import com.techvify.checkin.dto.RecognisedEmployeeDTO;
import com.techvify.checkin.dto.RecognisedUserDTO;
import com.techvify.checkin.dto.UserImageDto;
import com.techvify.checkin.entity.User;
import com.techvify.checkin.exception.ValidationException;
import org.flywaydb.core.internal.util.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class FacialRecognition {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Value("${facial-recognition.uri}")
    private String facialRecognitionUri;

    @Autowired
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    public String setupEmployeeForFacialRecognition(User user, List<String> imgs) {
        RestTemplate restTemplate = new RestTemplate();
        Long secondsTime = (new Date().getTime())/1000;
        try {
            return restTemplate.postForObject(
                    facialRecognitionUri + "/setup-employee",
                    new UserImageDto(user.getId(), user.getFirstName() + user.getLastName(), imgs, secondsTime),
                    String.class);
        } catch (Exception e) {
            logger.error("setupEmployeeForFacialRecognition(): {}", e.getMessage());
            throw new ValidationException("Employee facial recognition setup failed.");
        }
    }

    public String importImageForSetup(MultipartFile[] multipartFiles, Long userId) {
        RestTemplate restTemplate = new RestTemplate();
        Long secondsTime = (new Date().getTime())/1000;
        List<String> listBase64Encode = Arrays.stream(multipartFiles).map(x -> {
            try {
                return new String(Base64.getEncoder().encode(x.getBytes()));
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }).filter(x ->!StringUtils.isEmpty(x)).collect(Collectors.toList());
        User user = userService.getUserById(userId);
        try {
            return restTemplate.postForObject(
                    facialRecognitionUri + "/setup-employee",
                    new UserImageDto(user.getId(), user.getFirstName() + user.getLastName(), listBase64Encode, secondsTime),
                    String.class);
        } catch (Exception e) {
            logger.error("setupEmployeeForFacialRecognition(): {}", e.getMessage());
            throw new ValidationException("Employee facial recognition setup failed.");
        }
    }

    public RecognisedUserDTO verifyEmployeeByFacialRecognition(EmployeeFacialSetupDTO employeeFacialSetupDTO) {
        RecognisedEmployeeDTO recognisedEmployeeDTO = new RecognisedEmployeeDTO();
        RestTemplate restTemplate = new RestTemplate();
        employeeFacialSetupDTO.setSecondsTime((new Date().getTime())/1000);
        try {
            String result = restTemplate.postForObject(
                    facialRecognitionUri + "/verify-employee",
                    employeeFacialSetupDTO,
                    String.class);
            recognisedEmployeeDTO = objectMapper.readValue(result, RecognisedEmployeeDTO.class);
        } catch (Exception e) {
            logger.error("verifyEmployeeByFacialRecognition(): {}", e.getMessage());
        }

        String partId = recognisedEmployeeDTO.getPartId();
        if (StringUtils.isEmpty(partId)) {
            logger.error("GG_ERROR_FACIAL_RECOGNITION employeeId was empty");
        }
        Long userId = Long.parseLong(partId);
        User user = userService.getUserById(userId);

        //todo add threshold somewhere
        if (recognisedEmployeeDTO.getProbability() < 80) {
            logger.error("GG_ERROR_FACIAL_RECOGNITION threshold not met. The probability was {}", recognisedEmployeeDTO.getProbability());
        }

        RecognisedUserDTO recognisedUserDTO = RecognisedUserDTO.builder()
                .id(userId)
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .probability(recognisedEmployeeDTO.getProbability())
                .build();
        return recognisedUserDTO;
    }
}
